
package com.hibernate.example;

import java.util.ArrayList;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;


/**
 * @author Deepak Kumar
 *
 * http://www.roseindia.net
 * Hibernate example to inset data into Contact table
 */
public class One2ManyExample {
	public static void main(String[] args) {
		Session session = null;

		try{
			// This step will read hibernate.cfg.xml and prepare hibernate for use
			SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
			 session =sessionFactory.openSession();
				//Create new instance of Contact and set values in it by reading them from form object
			 	System.out.println("Inserting one2many Record for testing");
			 	Writer wr = new Writer();
			 	wr.setName("Das");
			 	ArrayList list = new ArrayList();
			 	list.add(new Story("Story Name 1"));
			 	list.add(new Story("Story Name 2"));
			 	wr.setStories(list);
			 	Transaction transaction = null;
			 	
			 	transaction = session.beginTransaction();
			 	session.save(wr);
			 	transaction.commit();
			 	System.out.println("Inserting one2many Done");
			 	
			 	
		
		}catch(Exception e){
			System.out.println(e);
			e.printStackTrace();
		}finally{
			// Actual contact insertion will happen at this step
			session.flush();
			session.close();

			}
	}	
	
}
