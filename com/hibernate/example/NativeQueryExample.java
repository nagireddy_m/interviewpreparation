package com.hibernate.example;

import org.hibernate.Session;
import org.hibernate.*;
import org.hibernate.criterion.*;
import org.hibernate.cfg.*;
import java.util.*;
/**
 * 
 * 
 * Hibernate Native Query Example
 *  
 */
public class NativeQueryExample {
	public static void main(String[] args) {
		Session session = null;

		try{
			// This step will read hibernate.cfg.xml and prepare hibernate for use
			SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
			session =sessionFactory.openSession();
			
 			/* Example to show Native query to select all the objects from database */
 			/* Selecting all the objects from insurance table */
 			List insurance = session.createSQLQuery("select  {a.*}  from insurance a")
			.addEntity("a", Insurance.class)
		    .list();
			for (Iterator it = insurance.iterator(); it.hasNext();) {
				Insurance insuranceObject = (Insurance) it.next();
				System.out.println("ID: " + insuranceObject.getLngInsuranceId());
				System.out.println("Name: " + insuranceObject.getInsuranceName());
			}
			
	        session.close();
		}catch(Exception e){
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
	}
}
