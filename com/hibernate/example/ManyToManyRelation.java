/**
 * 
 */
package com.hibernate.example;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 * @author 
 * 
 *
 */
public class ManyToManyRelation {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		SessionFactory sessFact = null;
		Session session = null;
		try {
			sessFact=new Configuration().configure().buildSessionFactory();
			session=sessFact.openSession();
			
			Transaction tr = session.beginTransaction();
		
			
			//One author and may books
			Author author = new Author();	
			author.setAuthorName("John");
			
			Author author2 = new Author();	
			author2.setAuthorName("Deepak");
			
			
			Set bookSet = new HashSet();
			//Solaris
			Book bookSolaris = new Book();
			bookSolaris.setBookName("Solaris in a week");
			bookSet.add(bookSolaris);
			
			//Linux
			Book bookLinux = new Book();
			bookLinux.setBookName("Linux in a week");
			bookSet.add(bookLinux);			
			
			//Oracle
			Book bookOracle = new Book();
			bookOracle.setBookName("Oracle in a week");
			bookSet.add(bookOracle);	
			
			//Add book set to the author object
			author.setBooks(bookSet);
			
			session.save(author);
			
			tr.commit();
			System.out.println("Done");
		}
		catch(HibernateException He){
			System.out.println(He.getMessage());
		}
		finally{
			session.close();
		}

	}

}



/*	

Book book = new Book();

book.setBookName("Phoenix");

Author author = new Author();
Author author1 = new Author();
Author author2 = new Author();
Author author3 = new Author();
author.setAuthorName("Clifford Geertz");
author1.setAuthorName("JP Morgenthal");
author2.setAuthorName("Yaswant Kanitkar");
author3.setAuthorName("Phola Pandit");

Set bookSet = new HashSet();
bookSet.add(book);
//bookSet.add(book1);
author.setBooks(bookSet);

//A book is written by many authors  
Set authorSet = new HashSet();
authorSet.add(author);
authorSet.add(author1);
book.setAuthors(authorSet);


Book book1 = new Book();
book1.setBookName("Enterprise Applications Integration with XML and Java");


Set authorSet1 = new HashSet();
authorSet1.add(author2);
authorSet1.add(author3);
book1.setAuthors(authorSet1);


sess.save(book);
//sess.save(book1);

sess.save(author);
*/

