package com.hibernate.example;

//Hibernate Imports
import java.util.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

//@Yaklur Mohammad

public class InsuranceIdIncrementExample {
	public static void main(String[] args) {
		Session session = null;

		try{
			// This step will read hibernate.cfg.xml and prepare hibernate for use
			SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
			session =sessionFactory.openSession();
			 
			org.hibernate.Transaction tx = session.beginTransaction();
			 
			//Create new instance of Insurance and set values in it by reading them from form object
		 	System.out.println("Inserting Insurance Details object into database..");
			Insurance insObject1  = new Insurance();
			insObject1.setInsuranceName("JeevanBheema");
			insObject1.setInvestementAmount(10000);
			insObject1.setInvestementDate(new Date("17-Jul-2012"));
			
			Insurance insObject2  = new Insurance();
			insObject2.setInsuranceName("Jeevan Anand");
			insObject2.setInvestementAmount(50000);
			insObject2.setInvestementDate(new Date("17-Jul-2012"));
			
			Insurance insObject3  = new Insurance();
			insObject3.setInsuranceName("ChildOne");
			insObject3.setInvestementAmount(20000);
			insObject3.setInvestementDate(new Date("17-Jul-2012"));
			
			
			
			session.save(insObject1); // only saves
			session.save(insObject2);
			session.save(insObject3);
			
			
			System.out.println("Insurance  object persisted to the database.");
	        tx.commit();
	        session.flush();
	        session.close();
		}catch(Exception e){
			System.out.println(e.getMessage());
		}finally{
			}
		
	}
}
