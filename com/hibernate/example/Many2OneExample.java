
package com.hibernate.example;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;


/**
 * Hibernate example to demostrate many to one
 */
public class Many2OneExample {
	public static void main(String[] args) {
		Session session = null;

		try{
			// This step will read hibernate.cfg.xml and prepare hibernate for use
			SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
			 session =sessionFactory.openSession();
				
			  
			 	
			 	//Query q = session.createSQLQuery("select id from story where id = 1");
			 	
				Query queryResult = session.createQuery("from Story");   
				List allStories = queryResult.list();   
				 
			    System.out.println("List size is "+allStories.size());
				Story story = (Story)allStories.get(0);
				Writer writer = story.getWriter();
				System.out.println("WriterName is "+writer.getName());
				
			 	
			 	
		
		}catch(Exception e){
			System.out.println(e);
			e.printStackTrace();
		}finally{
			// Actual contact insertion will happen at this step
			session.flush();
			session.close();

			}
	}	
	
}
