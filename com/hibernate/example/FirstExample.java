
package com.hibernate.example;

import java.util.List;
import java.util.Properties;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import org.hibernate.cfg.Configuration;

// This example to insert and retrive the contact details 

// Demo of From example as well

public class FirstExample {
	public static void main(String[] args) {
		Session session = null;

		try{
			// This step will read hibernate.cfg.xml and prepare hibernate for use
			  SessionFactory sessionFactory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
			  
			  
			  
			  
			    session =sessionFactory.openSession();
				//Create new instance of Contact and set values in it by reading them from form object
			 	System.out.println("Inserting Record for testing");
			 	Transaction tx = session.beginTransaction();

				Contact contact = new Contact(); // transient state
				contact.setFirstName("Test");
				contact.setLastName("Hibernate");
				contact.setEmail("test@yahoo.com");
								
				session.saveOrUpdate(contact); // persistant state 
				
				
				System.out.println("Successfully data insert in database");
				tx.commit();

				
				System.out.println("---Retriving the contents ---");
				
				// Now fetch the Record data from database			
			
				
				Query queryResult = session.createQuery("from Contact");   
				List allUsers = queryResult.list();   
				  
				  
				Contact user = (Contact) allUsers.get(0);
				System.out.println("Contact Details");
				System.out.println("---------------");
				System.out.println("ID "+user.getId());
				System.out.println("FirstName : "+user.getFirstName());
				System.out.println("LastName : "+user.getLastName());
				System.out.println("Email  : "+user.getEmail());
				     


		
		}catch(Exception e){
			System.out.println(e);
			e.printStackTrace();
		}finally{
			
			session.flush(); 
			session.close();  // its getting to detached state

			}
		
	}
}
