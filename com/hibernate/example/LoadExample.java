
package com.hibernate.example;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

// This example to insert and retrive the contact details 

// Demo of From example as well

public class LoadExample {
	public static void main(String[] args) {
		Session session = null;

		try{
			// This step will read hibernate.cfg.xml and prepare hibernate for use
			  SessionFactory sessionFactory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
			  
			  session =sessionFactory.openSession();
				
			 	System.out.println("Loading Record from Contact");
			 	
				// Now fetch the Record data from database			
				
				Contact contObj = (Contact)session.get(Contact.class,new Long(1));  
				
				System.out.println("Contact Details  ");
				System.out.println("-----------------");
				System.out.println("First Name is :"+contObj.getFirstName());
				System.out.println("Last Name is :"+contObj.getLastName());
				System.out.println("Email is :"+contObj.getEmail());


		
		}catch(Exception e){
			System.out.println(e);
			e.printStackTrace();
		}finally{
			// Actual contact insertion will happen at this step
			session.flush();
			session.close();

			}
		
	}
}
