
package com.hibernate.example;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;


/**
 * @author Yaklur M
 *
 * 
 */
public class UpdateExample {
	public static void main(String[] args) {
		Session session = null;

		try{
			// This step will read hibernate.cfg.xml and prepare hibernate for use
			SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
			 	session =sessionFactory.openSession();
				//Create new instance of Contact and set values in it by reading them from form object
			 	System.out.println("Update Example");
			 	Transaction tx = session.beginTransaction();
						 	
			 		 
				
				Contact cnt = new Contact();
				cnt.setId(new Long(1));			
				cnt.setFirstName("AAAA");
				cnt.setLastName("BBB");
				cnt.setEmail("a@b.com");
			 	
			   
			    session.update(cnt);  
				
			 	tx.commit();
				System.out.println("--Successfully Updated in database ------");
	
				// printing after updation
				Query queryResult = session.createQuery("from Contact");   
				List allUsers = queryResult.list();   
				  
				  
				Contact user = (Contact) allUsers.get(0);
				System.out.println("Contact Details");
				System.out.println("---------------");
				System.out.println("ID "+user.getId());
				System.out.println("FirstName : "+user.getFirstName());
				System.out.println("LastName : "+user.getLastName());
				System.out.println("Email  : "+user.getEmail());
				     
				
		}catch(Exception e){
			System.out.println(e);
			e.printStackTrace();
		}finally{
			// Actual contact updation will happen at this step
			session.flush();
			session.close();

			}
		
	}
	
	
}
